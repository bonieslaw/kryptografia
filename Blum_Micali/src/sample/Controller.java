package sample;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;


import java.awt.*;
import java.io.*;
import java.math.BigInteger;

public class Controller {

    @FXML
    private TextArea inputTextField;
    @FXML
    private TextArea outputTextField;
    @FXML
    private TextArea keyTextField;

    StreamCipher cipher = new StreamCipher();
    BlumMicaliNumberGenerator generator = new BlumMicaliNumberGenerator();

    @FXML
    private void switchInputAndOutputFieldContent() {
        String buffer = inputTextField.getText();
        inputTextField.setText(outputTextField.getText());
        outputTextField.setText(buffer);
    }

    @FXML
    private void cypherButtonClick() {
        String message = inputTextField.getText();
        String key = keyTextField.getText();
        String messageAsBinaryStream = convertStringToBinaryStream(message);
        String generatedNumberAsBinaryStream = generator.generate( key, BigInteger.valueOf( messageAsBinaryStream.length() ) );
        String cypheredMessageAsBinaryStream= cipher.cypherMessage(message, generatedNumberAsBinaryStream);

        outputTextField.setText(cypheredMessageAsBinaryStream);
    }

    private String convertStringToBinaryStream(String string) {
        String result = new BigInteger(string.getBytes()).toString(2);

        if(result.length() < 8) {
            result = "0" + result;
        }

        return result;
    }

    @FXML
    private void decryptButtonClick() {
        String cypheredMessage = inputTextField.getText();
        String key = keyTextField.getText();
        String generatedNumberAsBinaryStream = generator.generate( key, BigInteger.valueOf( cypheredMessage.length() ) );
        String decryptedMessageAsBinaryStream = cipher.decryptMessage(cypheredMessage, generatedNumberAsBinaryStream);
        String message = convertBinaryStringToString(decryptedMessageAsBinaryStream);

        outputTextField.setText(message);
    }

    private String convertBinaryStringToString(String string){
        StringBuilder sb = new StringBuilder();
        char[] chars = string.replaceAll("\\s", "").toCharArray();
        // mapping = {1,2,4,8,16,32,64,128};

        for (int j = 0; j < chars.length; j+=8) {
            int idx = 0;
            int sum = 0;
            //for each bit in reverse
            for (int i = 7; i >= 0; i--) {
                if (chars[i+j] == '1') {
                    sum += 1 << idx;
                }
                idx++;
            }
            sb.append(Character.toChars(sum));
        }
        return sb.toString();
    }

    @FXML
    private void cypherFileButtonClick() {
        String message = inputTextField.getText();
        String key = keyTextField.getText();
        FileChooser fileChooser = new FileChooser();
        Stage stage = new Stage();
        File file;
        Desktop desktop = Desktop.getDesktop();
        String fileAsString = "";

        fileChooser.setTitle("Wybierz plik");
        file = fileChooser.showOpenDialog(stage);
        try {
            fileAsString = FileUtils.readFileToString(file, "CP1252");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String messageAsBinaryStream = convertStringToBinaryStream(fileAsString);
        String generatedNumberAsBinaryStream = generator.generate( key, BigInteger.valueOf( messageAsBinaryStream.length() ) );
        String cypheredMessageAsBinaryStream= cipher.cypherMessage(fileAsString, generatedNumberAsBinaryStream);

        PrintWriter writer = null;
        try {
            writer = new PrintWriter("cyphered-file", "CP1252");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.print(cypheredMessageAsBinaryStream);
        writer.close();
    }

    @FXML
    private void decryptFileButtonClick() {
        String key = keyTextField.getText();
        FileChooser fileChooser = new FileChooser();
        Stage stage = new Stage();
        File file;
        String fileAsString = null;

        fileChooser.setTitle("Wybierz plik");
        file = fileChooser.showOpenDialog(stage);
        try {
            fileAsString = FileUtils.readFileToString(file, "CP1252");
        } catch (IOException e) {
            e.printStackTrace();
        }

        String generatedNumberAsBinaryStream = generator.generate( key, BigInteger.valueOf( fileAsString.length() ) );
        String decryptedMessageAsBinaryStream = cipher.decryptMessage(fileAsString, generatedNumberAsBinaryStream);
        String message = convertBinaryStringToString(decryptedMessageAsBinaryStream);

        PrintWriter writer = null;
        try {
            writer = new PrintWriter("encrypted-file","CP1252");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.print(message);
        writer.close();
    }
}

package sample;

import java.math.BigInteger;

public class StreamCipher {

    public String decryptMessage(String cypheredMessage, String keyAsBinaryStream) {
        String decryptedMessageAsBinaryStream = xorBinaryStreams(cypheredMessage, keyAsBinaryStream);

        //cypheredMessageAsHexStream = convertBinaryStreamToHexStream(cypheredMessageAsBinaryStream);

        return decryptedMessageAsBinaryStream;
    }

    public String cypherMessage(String message, String keyAsBinaryStream) {
        String messageAsBitStream = convertStringToBinaryStream(message);

        while( (messageAsBitStream.length() % 8) != 0 ) {
            messageAsBitStream = "0" + messageAsBitStream;
        }

        String cypheredMessageAsBinaryStream = xorBinaryStreams(messageAsBitStream, keyAsBinaryStream);

        return cypheredMessageAsBinaryStream;
    }

    private String convertStringToBinaryStream(String string) {
        return new BigInteger(string.getBytes()).toString(2);
    }

    private String convertHexStreamToBinaryStream(String hexString) {
        BigInteger hexInt = new BigInteger(hexString,16);

        String binaryStream = hexInt.toString(2);

        return binaryStream;
    }

    private String xorBinaryStreams(String first, String second) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < first.length(); i++) {
            sb.append(charOf(bitOf(first.charAt(i)) ^ bitOf(second.charAt(i) ) ) );
        }

        String result = sb.toString();

        return result;
    }

    private static char charOf(boolean in) {
        return (in) ? '1' : '0';
    }

    private static boolean bitOf(char in) {
        return (in == '1');
    }

    private String convertBinaryStreamToHexStream(String binaryStream) {
        BigInteger decimal = new BigInteger(binaryStream, 2);
        return decimal.toString(16);
    }
}

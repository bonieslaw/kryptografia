package sample;

import java.math.BigInteger;

import static java.lang.Math.pow;

public class BlumMicaliNumberGenerator {
    public String generate(String seedAsString, BigInteger numberOfIterations) {

        BigInteger primeNumber = BigInteger.valueOf(188249);
        //BigInteger primeNumber = BigInteger.valueOf(7);
        BigInteger generator = BigInteger.valueOf(3);
        BigInteger seed = BigInteger.valueOf( Integer.parseInt(seedAsString) );
        BigInteger buffer;
        String k = "";
        numberOfIterations = numberOfIterations.add(seed);

        for( BigInteger i = seed; i.compareTo(numberOfIterations) != 1; i = i.add(BigInteger.ONE) ) {
            buffer = powerBig(generator, seed).mod(primeNumber);
            if(seed.compareTo( ( primeNumber.add(BigInteger.valueOf(-1)).divide( BigInteger.valueOf(2) ) ) ) == -1 ) {
                k += "1";
            }
            else {
                k += "0";
            }

            seed = buffer;
        }
        return k;
    }

    private BigInteger powerBig(BigInteger number, BigInteger power) {
        if(power.compareTo(BigInteger.ZERO) == 0) {
            return BigInteger.ONE;
        }
        BigInteger result = number;

        while (power.compareTo(BigInteger.ONE) == 1) {
            result = result.multiply(number);
            power = power.add( BigInteger.valueOf(-1) );
        }
        return result;
    }
}
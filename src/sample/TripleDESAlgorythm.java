package sample;

import java.math.BigInteger;

public class TripleDESAlgorythm {
    private int blockLength;
    private String[] cn = new String[17];
    private String[] dn = new String[17];
    private String[] ln = new String[17];
    private String[] rn = new String[17];
    private String[] RnLn = new String[17];

    //przy czym pod indeksem 0 kryje się k1 itd.
    private String[] kn = new String[16];
    private int[] pc1;
    private int[] pc2;
    private int[] pTable;

    // tabelka - rząd - kolumna, numeracja tabel od 1.
    private int[][][] sTables;

    //E-bit selection table
    private int[] eTable;

    //initial permutation table
    private int[] ipTable;
    private String ip = new String();

    //liczba przesuniec bitowych klucza w lewo dla iteracji
    private int[] offsetsForShifts = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};

    public TripleDESAlgorythm() {
        this.blockLength = 64;
        this.pc1 = new int[] {  57, 49, 41, 33, 25, 17, 9,
                                1, 58, 50, 42, 34, 26, 18,
                                10, 2, 59, 51, 43, 35, 27,
                                19, 11, 3, 60, 52, 44, 36,
                                63, 55, 47, 39, 31, 23, 15,
                                7, 62, 54, 46, 38, 30, 22,
                                14, 6, 61, 53, 45, 37, 29,
                                21, 13, 5, 28, 20, 12, 4 };

        this.pc2 = new int[] {  14, 17, 11, 24, 1, 5,
                                3, 28, 15, 6, 21, 10,
                                23, 19, 12, 4, 26, 8,
                                16, 7, 27, 20, 13, 2,
                                41, 52, 31, 37, 47, 55,
                                30, 40, 51, 45, 33, 48,
                                44, 49, 39, 56, 34, 53,
                                46, 42, 50, 36, 29, 32 };

        this.ipTable = new int[] {  58, 50, 42, 34, 26, 18, 10, 2,
                                    60, 52, 44, 36, 28, 20, 12, 4,
                                    62, 54, 46, 38, 30, 22, 14, 6,
                                    64, 56, 48, 40, 32, 24, 16, 8,
                                    57, 49, 41, 33, 25, 17, 9, 1,
                                    59, 51, 43, 35, 27, 19, 11, 3,
                                    61, 53, 45, 37, 29, 21, 13, 5,
                                    63, 55, 47, 39, 31, 23, 15, 7 };

        this.eTable = new int[] {   32, 1, 2, 3, 4, 5,
                                    4, 5, 6, 7, 8, 9,
                                    8, 9, 10, 11, 12, 13,
                                    12, 13, 14, 15, 16, 17,
                                    16, 17, 18, 19, 20, 21,
                                    20, 21, 22, 23, 24, 25,
                                    24, 25, 26, 27, 28, 29,
                                    28, 29, 30, 31, 32, 1 };

        this.sTables = new int[][][] {
            {                       //S1
                {14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7},
                {0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8},
                {4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0},
                {15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13}
            },

            {                       //S2
                {15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10},
                {3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5},
                {0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15},
                {13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9}
            },

            {                       //S3
                {10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8},
                {13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1},
                {13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7},
                {1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12}
            },

            {                       //S4
                {7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15},
                {13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9},
                {10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4},
                {3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14}
            },

            {                       //S5
                {2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9},
                {14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6},
                {4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14},
                {11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3}
            },
            {                       //S6
                {12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11},
                {10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8},
                {9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6},
                {4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13}
            },

            {                       //S7
                {4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1},
                {13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6},
                {1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2},
                {6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12}
            },

            {                       //S8
                {13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7},
                {1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2},
                {7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8},
                {2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11}
            }
        };

        this.pTable = new int[] {   16, 7, 20, 21,
                                    29, 12, 28, 17,
                                    1, 15, 23, 26,
                                    5, 18, 31, 10,
                                    2, 8, 24, 14,
                                    32, 27, 3, 9,
                                    19, 13, 30, 6,
                                    22, 11, 4, 25 };
    }

    public String tripleDesCipher(String message, String firstKey, String secondKey, String thirdKey) {
        String cypheredMessageAsHexString;
        String messageAsBinaryString = convertMessageToBitString(message);

        messageAsBinaryString = cypherMessage(firstKey, messageAsBinaryString);
        messageAsBinaryString = decryptMessage(secondKey, messageAsBinaryString);
        messageAsBinaryString = cypherMessage(thirdKey, messageAsBinaryString);

        cypheredMessageAsHexString = convertBinaryStringToHexString(messageAsBinaryString);

        return cypheredMessageAsHexString;
    }

    public String tripleDesDecrypt(String messageAsHexString, String firstKey, String secondKey, String thirdKey) {
        String message;
        String messageAsBinaryString = convertHexStringToBinaryString(messageAsHexString);

        messageAsBinaryString = decryptMessage(thirdKey, messageAsBinaryString);
        messageAsBinaryString = cypherMessage(secondKey, messageAsBinaryString);
        messageAsBinaryString = decryptMessage(firstKey, messageAsBinaryString);

        message = convertBinaryStringToString(messageAsBinaryString);

        return message;
    }

    private String cypherMessage(String keyAsString, String messageAsBinaryString) {
        String cypheredMessageAsBinaryString = "";
        String[] blocks;

        //dopelnianie blokow do pelnych 64 bitow

        while( (messageAsBinaryString.length() % blockLength) != 0) {
            messageAsBinaryString = "0" + messageAsBinaryString;
        }
        blocks = messageAsBinaryString.split("(?<=\\G.{64})");
        for(int i = 0; i < blocks.length; i++) {
            cypheredMessageAsBinaryString += cypherBlock(keyAsString, blocks[i]);
        }

        return cypheredMessageAsBinaryString;
    }

    private String decryptMessage(String keyAsString, String cypheredMessageAsBinaryString) {
        String[] blocks;
        String decryptedMessageAsBitString = "";

        blocks = cypheredMessageAsBinaryString.split("(?<=\\G.{64})");

        for(int i = 0; i < blocks.length; i++) {
            decryptedMessageAsBitString += decryptBlock(keyAsString, blocks[i]);
        }
        return decryptedMessageAsBitString;
    }

    private String cypherBlock(String keyAsString, String block) {
        String reverseIp;
        String key = convertHexStringToBinaryString(keyAsString);
        String keyPlus = permuteBitStringWithTable(key, pc1);


        generateAllCnDn(keyPlus);
        permuteCnDnWithPC2();
        ip = permuteBitStringWithTable(block, ipTable);
        generateAllLnRn();
        createRnLnStrings();
        int[] reverseIpTable = reverseTable(ipTable);
        reverseIp = permuteBitStringWithTable(RnLn[16], reverseIpTable);

        return reverseIp;
    }

    private String convertHexStringToBinaryString(String hexString) {
        BigInteger hexInt = new BigInteger(hexString,16);

        String bitString = hexInt.toString(2);

        while ( (bitString.length() % blockLength) != 0 ) {
            bitString = "0" + bitString;
        }
        return bitString;
    }

    private String permuteBitStringWithTable(String bitString, int[] table) {
        String permutedBitString = "";

        for(int i = 0; i < table.length; i++) {
            permutedBitString += bitString.charAt(table[i] - 1);
        }
        return permutedBitString;
    }

    private void generateAllCnDn(String keyPlus) {
        setC0AndD0(keyPlus);
        for(int i = 1; i < cn.length; i++) {
            cn[i] = makeLeftShift(cn[i-1], offsetsForShifts[i-1]);
            dn[i] = makeLeftShift(dn[i-1], offsetsForShifts[i-1]);
        }
    }

    private void setC0AndD0(String keyPlus) {
        int i;
        cn[0] = "";
        dn[0] = "";
        for(i = 0; i < (keyPlus.length() / 2); i++ ) {
            cn[0] = cn[0] + keyPlus.charAt(i);
        }
        for(; i < keyPlus.length();  i++) {
            dn[0] = dn[0] + keyPlus.charAt(i);
        }
    }

    private void permuteCnDnWithPC2() {
        String CnDn;

        for(int i = 0; i < kn.length; i++) {
            kn[i] = "";
            CnDn = cn[i+1] + dn[i+1];
            for(int j = 0; j < pc2.length; j++) {
                kn[i] += CnDn.charAt(pc2[j] - 1);
            }
        }
    }

    private String convertMessageToBitString(String message) {
        return new BigInteger(message.getBytes()).toString(2);
    }

    private void generateAllLnRn() {
        setL0AndR0();

        for(int i = 1; i < ln.length; i++) {
            ln[i] = rn[i-1];
            rn[i] = xorStrings( ln[i - 1], fCalculation(rn[i-1], kn[i-1]) );
        }
    }

    private void setL0AndR0() {
        int i;
        ln[0] = "";
        rn[0] = "";

        for(i = 0; i < (ip.length() / 2); i++) {
            ln[0] = ln[0] + ip.charAt(i);
        }
        for(; i < ip.length(); i++) {
            rn[0] = rn[0] + ip.charAt(i);
        }
    }

    private void createRnLnStrings() {
        for(int i = 0; i < RnLn.length; i++) {
            RnLn[i] = rn[i] + ln[i];
        }
    }

    private int[] reverseTable(int[] table) {
        int[] reversedTable = new int[table.length];

        for(int i = 0; i < table.length; i++) {
            reversedTable[ table[i] - 1 ] = i + 1;
        }

        return reversedTable;
    }

    private String convertBinaryStringToHexString(String binaryString) {
        BigInteger decimal = new BigInteger(binaryString, 2);
        return decimal.toString(16);
}

    private String fCalculation(String r, String key) {
        String er = permuteWithETable(r);
        String ker = xorStrings(er, key);
        String gluedSBoxes = "";
        String result;

        // w obu przypadkach pod indeksem zero jest 1 element itd
        String[] bBoxes = new String[8];
        String[] sBoxes = new String[8];

        String rowBitString;
        String columnBitString;
        int rowNumber;
        int columnNumber;

        // wyplenianie blokow b
        int k = 0;

        for(int i = 0; i < bBoxes.length; i++, k+=6) {
            bBoxes[i] = "";
            for(int j = 0; j < 6; j++) {
                bBoxes[i] += ker.charAt(j+k);
            }
        }

        //permutacja blokow b na bloki s

        for(int i = 0; i < bBoxes.length; i++) {
            rowBitString = "";
            columnBitString = "";

            rowBitString += bBoxes[i].charAt(0);
            rowBitString += bBoxes[i].charAt( bBoxes[i].length() - 1 );
            rowNumber = Integer.parseInt(rowBitString, 2);

            for(int j = 1; j < 5; j++) {
                columnBitString += bBoxes[i].charAt(j);
            }
            columnNumber = Integer.parseInt(columnBitString, 2);

            sBoxes[i] = Integer.toBinaryString(sTables[i][rowNumber][columnNumber]);
            while (sBoxes[i].length() < 4) {
                sBoxes[i] = "0" + sBoxes[i];
            }
        }

        for(int i = 0; i < sBoxes.length; i++) {
            gluedSBoxes += sBoxes[i];
        }

        result = permuteWithPTable(gluedSBoxes);
        return result;
    }

    private String xorStrings(String first, String second) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < first.length(); i++) {
            sb.append(charOf(bitOf(first.charAt(i)) ^ bitOf(second.charAt(i) ) ) );
        }

        String result = sb.toString();

        return result;
    }

    private String permuteWithPTable(String toPermute) {
        String permuted = "";

        for(int i = 0; i < toPermute.length(); i++) {
            permuted += toPermute.charAt(pTable[i] - 1);
        }

        return permuted;
    }

    private String permuteWithETable(String toPermute) {
        String permuted = new String();

        for(int i = 0; i < eTable.length; i++) {
            permuted += toPermute.charAt(eTable[i] - 1);
        }

        return permuted;
    }

    private String makeLeftShift(String stringBit, int numberOfShifts) {
        String result = new String();

        for(int j = 0; j < numberOfShifts; j++) {
            result = "";
            for(int i = 0; i < stringBit.length(); i++) {
                if(i == stringBit.length() - 1) {
                    result = result + stringBit.charAt(0);
                }
                else {
                    result += stringBit.charAt(i + 1);
                }
            }
            stringBit = result;
        }
        return result;
    }

    private static boolean bitOf(char in) {
        return (in == '1');
    }

    private static char charOf(boolean in) {
        return (in) ? '1' : '0';
    }

    private String decryptBlock(String keyAsString, String block) {
        String blockAsBinaryString;
        int[] reverseIpTable = reverseTable(ipTable);

        String key = convertHexStringToBinaryString(keyAsString);
        String keyPlus = permuteBitStringWithTable(key, pc1);
        generateAllCnDn(keyPlus);
        permuteCnDnWithPC2();

        RnLn[16] = permuteBitStringWithTable(block, ipTable);
        reverseGenerateLnRn();
        ip = ln[0] + rn[0];
        blockAsBinaryString = permuteBitStringWithTable(ip, reverseIpTable);

        return blockAsBinaryString;
    }

    private void reverseGenerateLnRn() {
        separateR16L16();

        for(int i = ln.length - 1; i > 0; i--) {
            rn[i - 1] = ln[i];
            ln[i - 1] = xorStrings( rn[i], fCalculation(rn[i-1], kn[i-1]) );
        }
    }

    private void separateR16L16() {
        int i;

        rn[16] = "";
        ln[16] = "";

        for(i = 0; i < (RnLn[16].length() / 2); i++) {
            rn[16] += RnLn[16].charAt(i);
        }
        for(; i < RnLn[16].length(); i++) {
            ln[16] += RnLn[16].charAt(i);
        }
    }

    private String convertBinaryStringToString(String string){
        StringBuilder sb = new StringBuilder();
        char[] chars = string.replaceAll("\\s", "").toCharArray();
        int [] mapping = {1,2,4,8,16,32,64,128};

        for (int j = 0; j < chars.length; j+=8) {
            int idx = 0;
            int sum = 0;
            //for each bit in reverse
            for (int i = 7; i >= 0; i--) {
                if (chars[i+j] == '1') {
                    sum += 1 << idx;
                }
                idx++;
            }
            sb.append(Character.toChars(sum));
        }
        return sb.toString();
    }
}
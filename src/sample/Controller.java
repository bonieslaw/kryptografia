package sample;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.apache.commons.io.FileUtils;

import java.awt.*;
import java.io.*;

public class Controller {

    @FXML
    private TextArea inputTextField;
    @FXML
    private TextArea outputTextField;
    @FXML
    private TextArea key1TextField;
    @FXML
    private TextArea key2TextField;
    @FXML
    private TextArea key3TextField;

    private TripleDESAlgorythm desAlgorythm = new TripleDESAlgorythm();

    @FXML
    private void switchInputAndOutputFieldContent() {
        String buffer = inputTextField.getText();
        inputTextField.setText(outputTextField.getText());
        outputTextField.setText(buffer);
    }

    @FXML
    private void cypherButtonClick() {
        String key1AsString = key1TextField.getText();
        String key2AsString = key2TextField.getText();
        String key3AsString = key3TextField.getText();
        String inputTextAsString = inputTextField.getText();

        String cypher = desAlgorythm.tripleDesCipher(inputTextAsString, key1AsString, key2AsString, key3AsString);

        outputTextField.setText(cypher);
    }

    @FXML
    private void decryptButtonClick() {
        String inputTextFieldText = inputTextField.getText();
        String key1AsString = key1TextField.getText();
        String key2AsString = key2TextField.getText();
        String key3AsString = key3TextField.getText();
        String message = desAlgorythm.tripleDesDecrypt(inputTextFieldText, key1AsString, key2AsString, key3AsString);

        outputTextField.setText(message);
    }

    @FXML
    private void cypherFileButtonClick() {
        String key1AsString = key1TextField.getText();
        String key2AsString = key2TextField.getText();
        String key3AsString = key3TextField.getText();
        FileChooser fileChooser = new FileChooser();
        Stage stage = new Stage();
        File file;
        Desktop desktop = Desktop.getDesktop();
        String fileAsString = "";

        fileChooser.setTitle("Wybierz plik");
        file = fileChooser.showOpenDialog(stage);
        try {
            fileAsString = FileUtils.readFileToString(file, "CP1252");
        } catch (IOException e) {
            e.printStackTrace();
        }

        String cypheredFileAsString = desAlgorythm.tripleDesCipher(fileAsString, key1AsString, key2AsString, key3AsString);

        PrintWriter writer = null;
        try {
            writer = new PrintWriter("cyphered-file", "CP1252");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.print(cypheredFileAsString);
        writer.close();
        System.out.println("zaszyfrowano!");
    }

    @FXML
    private void decryptFileButtonClick() {
        String key1AsString = key1TextField.getText();
        String key2AsString = key2TextField.getText();
        String key3AsString = key3TextField.getText();

        FileChooser fileChooser = new FileChooser();
        Stage stage = new Stage();
        File file;
        String fileAsString = null;

        fileChooser.setTitle("Wybierz plik");
        file = fileChooser.showOpenDialog(stage);
        try {
            fileAsString = FileUtils.readFileToString(file, "CP1252");
        } catch (IOException e) {
            e.printStackTrace();
        }

        String decryptedFileAsString = desAlgorythm.tripleDesDecrypt(fileAsString, key1AsString, key2AsString, key3AsString);

        PrintWriter writer = null;
        try {
            writer = new PrintWriter("encrypted-file","CP1252");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.print(decryptedFileAsString);
        writer.close();
        System.out.println("odszyfrowano!");
    }

    private String convertBinaryStringToString(String string){
        StringBuilder sb = new StringBuilder();
        char[] chars = string.replaceAll("\\s", "").toCharArray();
        int [] mapping = {1,2,4,8,16,32,64,128};

        for (int j = 0; j < chars.length; j+=8) {
            int idx = 0;
            int sum = 0;
            //for each bit in reverse
            for (int i = 7; i >= 0; i--) {
                if (chars[i+j] == '1') {
                    sum += 1 << idx;
                }
                idx++;
            }
            sb.append(Character.toChars(sum));
        }
        return sb.toString();
    }
}
